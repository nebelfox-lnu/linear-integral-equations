from typing import Callable, Collection
import numpy as np

F1 = Callable[[float], float]


def quad_midpoint_even(a: float,
                       b: float,
                       f: F1,
                       n: int) -> float:
    return quad_midpoint(np.linspace(a, b, n), f)


def quad_midpoint(partitioning: Collection[float],
                  f: F1) -> float:
    return sum(
        f(start + (end - start) * 0.5) * (end - start)
        for start, end in zip(partitioning, partitioning[1:])
    )
