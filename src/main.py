from dataclasses import dataclass
from math import *
from typing import Callable, Optional, List

import click
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from galerkin import galerkin

F1 = Callable[[float], float]
F2 = Callable[[float, float], float]


def make_f1(expr: str) -> F1:
    return eval(f'lambda x: {expr}')


def make_f2(expr: str) -> F2:
    return eval(f'lambda x, y: {expr}')


def plot(a: float,
         b: float,
         n: int,
         approximation: F1,
         approximation_n: int,
         analytical: Optional[F1]) -> None:
    xs = np.linspace(a, b, n)
    ys_phi = list(map(approximation, xs))
    plt.plot(xs, ys_phi, '-', alpha=0.5,
             label=f'φ_{approximation_n}(x)')
    if analytical is not None:
        ys_analytical = list(map(analytical, xs))
        plt.plot(xs, ys_analytical, '--', alpha=0.5, label='φ(x)')
    plt.grid()
    plt.legend()
    plt.title(f'φ_{approximation_n}(x)')
    plt.show()


def get_max_error(f1: F1, f2: F1, a: float, b: float, n: int):
    return max(((x, abs(f1(x) - f2(x)))
                for x in np.linspace(a, b, n)),
               key=lambda xy: xy[1])


@click.command(help="""
Solve a Fredholm second kind integral equation like

\b
           b
    φ(x) - ∫ φ(y) K(x, y) dy = f(x)
           a

Or represented alternatively as 

\b
                  b
    φ(x) = f(x) + ∫ φ(y) K(x, y) dy
                  a

Where f(x) and K(x, y) are given functions using Galerkin method 
with linear splines and midpoint quadrature method.
The approximation is searched as

\b
             n
    φ_n(x) = Σ c_j l_j(x)
            j=0

Where 

\b
             | (x - x_{j-1}) / h, x in [x_{j-1}, x_j], j > 0
    l_j(x) = | (x_{j+1} - x) / h, x in [x_j, x_{j+1}], j < n
             | 0 otherwise

\b
    h = (b - a)/n

\b
    x_j = a + jh, j=0,...,n
    
And {c_j, j=0,...,n} is the solution of the linear equations system

\b
    n                    b b                                 b
    Σ c_j [ (l_i, l_j) - ∫ ∫ l_i(x) l_j(y) K(x, y) dy dx ] = ∫ l_i(x) f(x) dx, i=0,...,n
   j=0                   a a                                 a


Where

\b
                  b
    (l_i, l_j) =  ∫ l_i(x) l_j(x) dx
                  a
""")
@click.option('-a', type=float, required=True,
              help='Lower bound of the integral')
@click.option('-b', type=float, required=True,
              help='Upper bound of the integral')
@click.option('-f', type=str, required=True, metavar='EXPR',
              help='Function f(x): R -> R')
@click.option('-K', '--kernel', type=str, required=True, metavar='EXPR',
              help='Function K(x, y): R^2 -> R')
@click.option('-n', '--ns', type=lambda s: list(map(int, s.split(','))),
              metavar='LIST_OF_INTS', required=True,
              help='Comma-separated list of sub-intervals number '
                   'to use in the Galerkin method')
@click.option('-m', type=int, required=True, default=32, show_default=True,
              help='Number of knots to use in midpoint quadrature method '
                   'for calculating integrals')
@click.option('--analytical', type=make_f1, metavar='EXPR',
              help='Analytical solution of the equation '
                   'to compare the obtained approximation with')
@click.option('-s', '--error-sample-size', type=int, default=1000,
              help='Number of evenly spaced points on [a, b] to calculate'
                   'the max error')
@click.option('-e', '--error-at',
              type=lambda s: list(map(float, s.split(','))), metavar='XS',
              required=False,
              help='Comma-separated list of x-values to calculate'
                   'the error as difference between the analytical and '
                   'approximate solutions at')
@click.option('--plot-sample-size', type=int,
              help='Draw a plot of each approximation using such number of '
                   'evenly spaced points from [a, b]')
@click.option('-A', '--print-A', is_flag=True, default=False,
              show_default=True, help='Show matrix A in the output')
@click.option('-B', '--print-B', is_flag=True, default=False,
              show_default=True, help='Show vector B in the output')
@click.option('-C', '--print-C', is_flag=True, default=False,
              show_default=True, help='Show vector C in the output')
@click.option('--max-error-plot', is_flag=True, default=False,
              show_default=True, help='Show plot of n to MaxError(φ_n(x))')
def main(a: float,
         b: float,
         f: str,
         kernel: str,
         ns: List[int],
         m: int,
         analytical: Optional[F1],
         error_sample_size: int,
         error_at: Optional[List[float]],
         plot_sample_size: Optional[int],
         print_a: bool,
         print_b: bool,
         print_c: bool,
         max_error_plot: bool) -> None:
    np.set_printoptions(linewidth=np.inf)

    print(f"""
Approximating the solution of the following equation:
    
                  b
    φ(x) = f(x) + ∫ φ(y) K(x, y) dy
                  a

Where 
    f(x) = {f}
    K(x, y) = {kernel}""")

    max_errors = []
    phi_ns = []
    errors_at = dict()
    values_at = {x: [] for x in error_at}

    for i, n in enumerate(ns):
        m = max(m, n*2)
        print('\n' + '=' * 100)
        print(f'Approximating with {n=} {m=}')
        phi_n, A, B, C = galerkin(a, b, make_f1(f), make_f2(kernel), n, m)

        if print_a:
            print('Matrix A:')
            print(A)
        if print_b:
            print('Vector B:')
            print(B)
        if print_c:
            print('Vector C:')
            print(C)

        max_error_x, max_error = get_max_error(phi_n, analytical, a, b,
                                               error_sample_size)
        print(f'MaxError(φ_{n}) := max(|φ(x) - φ_{n}(x)|) '
              f'= {max_error} at x={max_error_x}; '
              f'φ({max_error_x})={analytical(max_error_x)}; '
              f'φ_{n}({max_error_x})={phi_n(max_error_x)}')
        if max_errors:
            last_error = max_errors[-1]
            print(f'MaxError(φ_{n}) = {max_error / last_error} * MaxError(φ_{ns[i-1]})')
        max_errors.append(max_error)

        if analytical and error_at is not None:
            print('\nErrors at specified points:')
            errors_df = pd.DataFrame([
                {
                    'x': x,
                    'exact': analytical(x),
                    f'approx': phi_n(x)
                }
                for x in error_at
            ])
            errors_df['error'] = (errors_df['exact'] - errors_df[f'approx']).abs()
            errors_at[n] = list(errors_df['error'])
            for i, row in errors_df.iterrows():
                values_at[row['x']].append(row['approx'])
            errors_df.rename({'exact': 'φ'})
            print(errors_df.rename({'exact': 'φ(x)', 'approx': f'φ_{n}(x)'}))
            print(errors_df.describe().drop(
                columns=['x', 'exact', f'approx']))
        phi_ns.append(phi_n)

    print('')
    errors_df = pd.DataFrame([
        dict(n=n, **{
            f'{x=}': error
            for x, error in zip(error_at, errors)
        })
        for n, errors in errors_at.items()
    ])
    print(errors_df)





    if plot_sample_size:
        for i, n in enumerate(ns):
            plot(a, b, plot_sample_size, phi_ns[i], n, analytical)

    if max_error_plot:
        plt.plot(ns, max_errors, 'b-*')
        for n, max_error in zip(ns, max_errors):
            plt.text(n, max_error, str(n))
        plt.xlabel('n')
        plt.ylabel('MaxError(φ_n)')
        plt.title('Dependency of MaxError(φ_n) from n')
        plt.show()

    for x, ys in values_at.items():
        y = analytical(x)
        plt.plot(list(range(len(ns))), [y] * len(ns), '-', label=f'φ({x})')
        plt.plot(list(range(len(ns))), ys, '--', label=f'φ_n({x})')
        plt.xticks(ticks=list(range(len(ns))), labels=[f'{n=}' for n in ns])
        plt.legend()
        plt.show()


if __name__ == '__main__':
    main(max_content_width=168)
