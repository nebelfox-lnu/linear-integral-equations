from typing import Tuple, Sequence, Callable, List

F1 = Callable[[float], float]
Point = Tuple[float, float]


def make_basis_function(j: int, xs: List[float]) -> F1:
    def basis_function(x: float) -> float:
        if j > 0 and xs[j-1] <= x <= xs[j]:
            return (x - xs[j-1]) / (xs[j] - xs[j-1])
        elif j+1 < len(xs) and xs[j] <= x <= xs[j+1]:
            return (xs[j+1] - x) / (xs[j+1] - xs[j])
        return 0

    return basis_function


def make_linear_spline_basis(nodes: Sequence[Point]) -> List[F1]:
    xs = [node[0] for node in nodes]
    return [
        make_basis_function(j, xs)
        for j in range(len(xs))
    ]
