from typing import Callable, Tuple
from pathos.multiprocessing import ProcessingPool as Pool
from os import cpu_count

import numpy as np
from numpy.typing import NDArray

from linear_spline import make_linear_spline_basis
from quad import quad_midpoint


F1 = Callable[[float], float]
F2 = Callable[[float, float], float]


def galerkin_petrov(a: float,
             b: float,
             f: F1,
             K: F2,
             n: int,
             m: int) -> Tuple[F1, NDArray, NDArray, NDArray]:
    xs = np.linspace(a, b, n + 1)
    ys = map(f, xs)
    nodes = list(zip(xs, ys))
    A = np.zeros((n + 1, n + 1))
    B = np.zeros(n + 1)
    phis = make_linear_spline_basis(nodes)
    integration_partitioning = np.linspace(a, b, m)

    def integrate(f: F1) -> float:
        return quad_midpoint(integration_partitioning, f)

    for i in range(n + 1):
        B[i] = integrate(lambda x: phis[i](x) * integrate(lambda s: K(x, s) * f(s)))

        for j in range(n + 1):
            first = integrate(lambda x: phis[i](x) * phis[j](x))
            second = integrate(lambda x: phis[j](x) * integrate(lambda s: K(x, s) * phis[i](s)))
            A[i, j] = first - second

    C = np.linalg.solve(A, B)

    def phi(x: float) -> float:
        return f(x) + sum(ci * phis[i](x) for i, ci in enumerate(C))

    return phi, A, B, C


def galerkin_bubnov(a: float,
                    b: float,
                    f: F1,
                    K: F2,
                    n: int,
                    m: int) -> Tuple[F1, NDArray, NDArray, NDArray]:
    h = (b - a) / n
    xs = np.linspace(a, b, n + 1)
    ys = map(f, xs)
    nodes = list(zip(xs, ys))
    A = np.zeros((n + 1, n + 1))
    B = np.zeros(n + 1)
    phis = make_linear_spline_basis(nodes)
    integration_partitioning = np.linspace(a, b, m)

    def integrate(f: F1) -> float:
        return quad_midpoint(integration_partitioning, f)

    for i in range(n + 1):
        B[i] = integrate(lambda x: phis[i](x) * integrate(lambda s: K(x, s) * f(s)))

        for j in range(n + 1):
            alpha = integrate(lambda x: phis[i](x) * phis[j](x))
            beta = integrate(lambda x: phis[i](x) * integrate(lambda s: K(x, s) * phis[j](s)))
            A[i, j] = alpha - beta

    C = np.linalg.solve(A, B)

    def phi(x: float) -> float:
        return f(x) + sum(ci * phis[i](x) for i, ci in enumerate(C))

    return phi, A, B, C


def galerkin(a: float,
             b: float,
             f: F1,
             K: F2,
             n: int,
             m: int) -> Tuple[F1, NDArray, NDArray, NDArray]:
    xs = np.linspace(a, b, n + 1)
    ys = map(f, xs)
    nodes = list(zip(xs, ys))
    A = np.zeros((n + 1, n + 1))
    B = np.zeros(n + 1)
    phis = make_linear_spline_basis(nodes)
    psis = phis
    integration_partitioning = np.linspace(a, b, m)

    def integrate(f: F1) -> float:
        return quad_midpoint(integration_partitioning, f)

    def calc_bi(i: int) -> float:
        return integrate(lambda x: psis[i](x) * f(x))

    def calc_aij(i: int, j: int) -> float:
        first = integrate(lambda x: phis[i](x) * psis[j](x))
        second = integrate(lambda x: integrate(lambda y: phis[i](x) * K(x, y) * phis[j](y)))
        return first - second

    with Pool(cpu_count()) as pool:
        B = np.array(pool.map(calc_bi, range(n+1)), copy=True)
        A_flat = np.array(
            pool.map(calc_aij,
                     sum([[i] * (n+1) for i in range(n+1)], []),
                     list(range(n+1)) * (n + 1)),
            copy=True
        )
        A = np.zeros((n+1, n+1))
        for i in range(n+1):
            for j in range(n+1):
                A[i, j] = A_flat[i*(n+1) + j]

    C = np.linalg.solve(A, B)

    def phi(x: float) -> float:
        return sum(ci * phis[i](x) for i, ci in enumerate(C))

    return phi, A, B, C
